#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""

from lxml import etree
from itertools import chain
import os
from zipfile import ZipFile
import json

class CongressXml():
	def __init__(self, file_path):
		self.file_path = file_path
		self.errors = 0


	def ExtractToFiles(self):
		with open(self.file_path, "rb") as f:
			count = 0
			for i, line in enumerate(f):
				if "<result>" in line:
					self.line = line
					count += 1
					tree = self.BuildTree(line)
					self.date = self.GetData(tree, "/result/date")
					self.title = self.GetData(tree, "/result/title")
					if not self.CheckDir(os.path.dirname(self.file_path)):
						self.MakeDir(os.path.dirname(self.file_path))
					year_path = os.path.join(os.path.dirname(self.file_path), "congressional_by_year", self.date[0:4])
					self.CopyXml(year_path)
					self.CreateTxt(year_path, tree)
					if count % 500 == 0:
						print "{0} Documents Complete".format(count)
		print "Total Errors: {0}".format(self.errors)

	def BuildTree(self, line):
		self.text = line.replace("\\n", "")
		tree = etree.fromstring(self.text)
		return tree

	def GetData(self, tree, xpath):
		result = tree.xpath(xpath)
		if result:
			result_text = result[0].text

		else:
			result_text = "NONE"
			print "ERROR: Missing {0} for {1}. Contains these elements:".format(xpath, self.text[:100])
			print "ERROR", [x.tag for x in tree.getchildren()]
			print "ERROR", self.line
			self.errors += 1
		return result_text

	def CheckDir(self, path):
		return os.path.isdir(os.path.join(path, "congressional_by_year", self.date[0:4]))

	def MakeDir(self, path):
		new_dir = os.path.join(path, "congressional_by_year", self.date[0:4])
		os.mkdir(new_dir)

	def CopyXml(self, path):
		new_file = os.path.join(path, self.title.replace(" ", "_")+".xml")
		with open(new_file, "w") as g:
			g.write('<?xml version="1.0" encoding="UTF-8"?>')
			g.write(self.text)

	def CreateTxt(self, path, tree):
		new_file = os.path.join(path, self.title.replace(" ", "_")+".txt")
		full_text = unicode(self.GetData(tree, "/result/fulltext"))
		with open(new_file, "w") as g:
			g.write(full_text.encode('utf8'))

	def MakeZip(self, file_ending):
		file_dir = os.path.join(os.path.dirname(self.file_path), "congressional_by_year")
		#files_lists = (os.listdir(os.path.join(file_dir, f)) for f in os.listdir(file_dir))
		#files_all = chain.from_iterable(files_lists)
		file_dir_name = "congressional_"+file_ending+"_files"
		with ZipFile(os.path.join(os.path.dirname(self.file_path), file_dir_name+".zip"), "w", allowZip64=True) as azip:
			for d in os.listdir(file_dir):
				for f in os.listdir(os.path.join(file_dir, d)):
					if f.endswith(file_ending):
						azip.write(os.path.join(file_dir, d, f), os.path.join(file_dir_name, d, f))
		

	def ZipByYear(self, file_ending):
		dirs = [d[0] for d in os.walk(self.file_path)]
		for dir_year in dirs[1:]:
			print dir_year
			zipped_filename = os.path.basename(dir_year)+"_"+file_ending+".zip"
			with ZipFile(os.path.join(dir_year, zipped_filename), "w", allowZip64=True) as azip:
				for d in os.listdir(dir_year):
					if d.endswith(file_ending):
						file_path = os.path.join(dir_year, d)
						azip.write(file_path, os.path.basename(file_path))

	def BuildDirectory(self, make_csv=True, make_json=True):
		congressional_directory = {}
		csv_lines = []
		with open(self.file_path, "rb") as f:
			count = 0
			for i, line in enumerate(f):
				if "<result>" in line:
					tree = self.BuildTree(line)
					self.title = self.GetData(tree, "/result/title")
					self.date = self.GetData(tree, "/result/date")
					self.year = self.date[0:4]
					self.filename = self.title.replace(" ", "_")
					self.pdf = self.GetData(tree, "/result/pdf")
					if make_csv:
						csv_lines.append(",".join([self.year, self.filename, self.pdf+"\n"]))

					if self.year in congressional_directory:
						congressional_directory[self.year][self.filename] = self.pdf
					else:
						congressional_directory[self.year] = {}
						congressional_directory[self.year][self.filename] = self.pdf

		if make_json:
			with open(os.path.join(os.path.dirname(self.file_path), "congressional_directory.json"), "w") as g:
				data = json.dump(congressional_directory, g)
			
		if make_csv:
			with open(os.path.join(os.path.dirname(self.file_path), "congressional_directory2.csv"), "a") as g:
				for line in sorted(csv_lines):
					g.write(line)

	def TestPdf(self):
		with open(self.file_path, "rb") as f:
			for line in f:
				if "<result>" in line:
					self.line = line
					tree = self.BuildTree(line)
					self.pdf = self.GetData(tree, "/result/fulltext")
					if self.pdf == "NONE":
						print line






